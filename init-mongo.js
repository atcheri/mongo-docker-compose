// const user = process.env.MONGO_USERNAME;
// const pwd = process.env.MONGO_PASSWORD;
// const db = process.env.MONGO_DATABASE;

db.auth("admin-user", "admin-password");

const user = "db-username";
const pwd = "db-password";
const db = "database-name";

db.createUser({
  user,
  pwd,
  roles: [
    {
      role: "readWrite",
      db,
    },
  ],
});
